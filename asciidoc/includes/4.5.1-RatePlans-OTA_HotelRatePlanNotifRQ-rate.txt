<!-- following: "normal" rates ... -->

<Rate InvTypeCode="double" Start="2014-03-03" End="2014-03-08">
<BaseByGuestAmts>
  <BaseByGuestAmt NumberOfGuests="1" AgeQualifyingCode="10" AmountAfterTax="106"/>
  <BaseByGuestAmt NumberOfGuests="2" AgeQualifyingCode="10" AmountAfterTax="96"/>
</BaseByGuestAmts>
<AdditionalGuestAmounts>
  <AdditionalGuestAmount AgeQualifyingCode="10"                         Amount="76.8"/>
  <AdditionalGuestAmount AgeQualifyingCode="8"              MaxAge="3"  Amount="0"   />
  <AdditionalGuestAmount AgeQualifyingCode="8"  MinAge="3"  MaxAge="6"  Amount="38.4"/>
  <AdditionalGuestAmount AgeQualifyingCode="8"  MinAge="6"  MaxAge="10" Amount="48"  />
  <AdditionalGuestAmount AgeQualifyingCode="8"  MinAge="10" MaxAge="16" Amount="67.2"/>
</AdditionalGuestAmounts>
</Rate>
